contact_fields = ["first_name", "last_name", "email", "phone", "address"];
DEFAULT_PIC="http://localhost:3000/grey_silhouette.png"; //change the url later
modalOptions = {
      dismissible: false,
      ready: function(){
        checkBackground();
        disableSaveButton();
      },
      complete: function(){
        Session.set("currentContact", null);
        $.each(contact_fields, function(index, value){
          $("#"+value).val(null);
          $("#"+value).removeClass("valid");
          $("#"+value).siblings().removeClass("active");
        })
        $("#profile_image_input").val(null);
        $("#profile_image").css("background-image", "url('"+DEFAULT_PIC+"')");
        checkBackground();
      }
    }
var checkBackground = function(){
  if(getUrl()==DEFAULT_PIC){
    if(!$("#remove-pic-button").hasClass("disabled"))
      $("#remove-pic-button").addClass("disabled");
  }
  else
    $("#remove-pic-button").removeClass("disabled");
  if(currentContact)
    if($(".mdi-content-save").parent().hasClass("disabled")){
      if((getUrl() != currentContact["image"]) && (!($("#first_name").val().trim().length == 0)))
        $(".mdi-content-save").parent().removeClass('disabled');
    }
    else if((getUrl() == currentContact["image"]) || (getUrl()==DEFAULT_PIC && currentContact["image"]==undefined)){
      disableSaveButton();
    }
}
var disableSaveButton = function(){
  $(".mdi-content-save").parent().removeClass('disabled');
  $(".mdi-content-save").parent().addClass('disabled'); 
}
var getUrl = function(){
  return $("#profile_image").css('background-image').replace('url(','').replace(')','');
}