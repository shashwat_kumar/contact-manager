Template.contact_details.helpers({
	contact: function(){
	  return Contacts.findOne(Session.get("currentContact"));
	}
})
Template.contact_details.events({
	"click .mdi-content-save": function(event){
		if($(event.target).parent().hasClass("disabled"))
			return false;
	  updatedContact = {}
	  $.each(contact_fields, function(index, value){
	    updatedContact[value] = $("#"+value).val().trim();
	  })
	  if(getUrl()!=DEFAULT_PIC)
	    updatedContact["image"] = getUrl();
	  else
	    delete updatedContact["image"];
	  if(Session.get("currentContact"))
	    Meteor.call("updateContact", Session.get("currentContact"), updatedContact);
	  else{
	    updatedContact["created_at"] = new Date();
	    updatedContact["owner"] = Meteor.userId();
	    Meteor.call("addContact", updatedContact, function(error, result){
	      Session.set("currentContact", result);
	    })
	  }
	  disableSaveButton();
	},
	"click #remove-pic-button": function(event){
		if($(event.target).hasClass("disabled"))
			return false;
	  $("#profile_image").css("background-image", "url('"+DEFAULT_PIC+"')");
	  checkBackground();
	},
	"input input": function(event){
	  result = false;
	  if(currentContact)
	    $.each(contact_fields, function(index, value){
	      result = result || (currentContact[value] != $("#"+value).val())
	    })
	  else
	    result = true;
	  if((result||((getUrl() != currentContact["image"])&&(!(getUrl()==DEFAULT_PIC && currentContact["image"]==undefined)))) && !($("#first_name").val().trim().length == 0))
	    $(".mdi-content-save").parent().removeClass('disabled');
	  else
	    disableSaveButton();
	},
	'click #profile_image': function(){
	  $("#profile_image_input").click();
	},
	'change #profile_image_input': function(event, template) {
	  if(event.target.files && event.target.files[0]){
	    var reader = new FileReader();
	    reader.onload = function(e){
	      $("#profile_image").css("background-image", "url("+e.target.result+")");
	      checkBackground()
	    }
	    reader.readAsDataURL(event.target.files[0]);
	  }
	}
})