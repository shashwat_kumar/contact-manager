Template.contact.helpers({
	isOwner: function(){
	  return this.owner === Meteor.userId();
	}
});
Template.contact.events({
	"click .delete-btn": function(event){
	  var contactId = this._id;
	  $(event.target).parent().parent().fadeOut("slow", function(){
	    Meteor.call("removeContact", contactId);
	    Session.set("currentContact", null);
	  });
	}
});
Template.contact.rendered = function(){
	$(".modal-trigger").leanModal(modalOptions);
};